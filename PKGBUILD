# Maintainer: Santiago Torres-Arias <santiago@archlinux.org>
# Contributor: Christian Rebischke <chris.rebischke@archlinux.org>
# Contributor: Timothy Redaelli <timothy.redaelli@gmail.com>
# Contributor: Andy Weidenbaum <archbaum@gmail.com>

pkgname=electrum
pkgver=4.5.4
pkgrel=1
pkgdesc="Lightweight Bitcoin wallet"
arch=('any')
makedepends=('python-setuptools')
depends=('python-pyaes' 'python-ecdsa' 'python-pbkdf2' 'python-requests' 'python-qrcode'
         'python-protobuf' 'python-dnspython' 'python-jsonrpclib-pelix' 'python-pysocks'
         'python-pyqt5' 'python-pycryptodomex' 'python-websocket-client' 'python-certifi'
         'python-aiorpcx' 'python-aiohttp' 'python-aiohttp-socks'
         'libsecp256k1' 'python-bitstring' 'python-jsonpatch')
optdepends=('python-btchip: BTChip hardware wallet support'
            'python-hidapi: Digital Bitbox hardware wallet support'
            'python-matplotlib: plot transaction history in graphical mode'
            'zbar: QR code reading support'
            'python-rpyc: send commands to Electrum Python console from an external script'
            'python-qdarkstyle: optional dark theme in graphical mode'
            'python-pycryptodomex: use PyCryptodome AES implementation instead of pyaes'
         )

url="https://electrum.org"
license=('MIT')
source=(https://download.electrum.org/${pkgver}/${pkgname^}-${pkgver}.tar.gz{,.asc})
sha512sums=('3b533b0482f6757a2863b133c0026bac05456fa608d5773a5a53b5f99c3950ae6ab0e0b8f18aae03dcfe6ebee49b8b58389f7e3436edccde09049b6166fe8928'
            'SKIP')
validpgpkeys=('6694D8DE7BE8EE5631BED9502BD5824B7F9470E6'  # ThomasV 
              '0EEDCFD5CAFB459067349B23CA9EEEC43DF911DC'  # SomberNight
              '9EDAFF80E080659604F4A76B2EBB056FD847F8A7') # Emzy 

prepare() {
  cd "${pkgname^}-${pkgver}"
}

build() {
  cd "${pkgname^}-${pkgver}"

  python setup.py build
}

package() {
  cd "${pkgname^}-${pkgver}"

  python setup.py install --root="${pkgdir}" --optimize=1
  install -D -m644 LICENCE "${pkgdir}"/usr/share/licenses/"${pkgname}"/LICENSE
}
